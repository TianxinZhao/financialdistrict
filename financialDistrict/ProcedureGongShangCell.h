//
//  ProcedureGongShangCell.h
//  financialDistrict
//
//  Created by USTB on 13-4-7.
//  Copyright (c) 2013年 USTB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProcedureGongShangCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *procedureIcon;
@property (weak, nonatomic) IBOutlet UILabel *procedureTitle;



@end
