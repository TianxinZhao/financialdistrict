//
//  GovViewController.h
//  financialDistrict
//
//  Created by USTB on 13-3-26.
//  Copyright (c) 2013年 USTB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GovViewController : UIViewController

@property (nonatomic,strong) NSMutableArray *infoMenu;
@property (nonatomic,strong) NSDictionary *stringsDictionary;


@end
