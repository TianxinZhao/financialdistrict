//
//  ProductInfoViewController.h
//  jrj
//
//  Created by jrj on 13-3-25.
//  Copyright (c) 2013年 jrj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductInfoViewController : UIViewController

@property id item;

@property IBOutlet UILabel *name;
@property IBOutlet UIWebView *webView;

@end
