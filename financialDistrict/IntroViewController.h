//
//  IntroViewController.h
//  financialDistrict
//
//  Created by USTB on 13-3-11.
//  Copyright (c) 2013年 USTB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *introText;

@end
