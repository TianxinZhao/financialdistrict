//
//  YuXiangCell.h
//  financialDistrict
//
//  Created by USTB on 13-3-14.
//  Copyright (c) 2013年 USTB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YuXiangCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *yuXiangImage;
@property (weak, nonatomic) IBOutlet UILabel *yXTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *yXContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *yXAddressLabel;

@end
